package main

import (
	"flag"

	"gitlab.com/pbacterio/home_sd/internal/homesd"
)

func main() {
	portFlag := flag.Int("port", 8080, "Listening port.")
	flag.Parse()
	homesd.Server(*portFlag)
}
