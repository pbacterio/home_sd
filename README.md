HOME_SD - Home (network) node discovery 
=========================================

HOME_SD is a Prometheus node_exporter service discovery for home networks.

This software is made just to cover my personal requirements. It is intended to dynamically register computers (not always on, no fixed ip) on Prometheus.


## Setup

Download the binary from https://pbacterio.gitlab.io/home_sd

Give it executable permissions.
    
    chmod +x home_sd

Running as a service for example with systemd:

    [Unit]
    Wants=network-online.target
    After=network-online.target

    [Service]
    Type=simple
    ExecStart=/usr/local/bin/home_sd --port 40404

    [Install]
    WantedBy=multi-user.target



## Prometheus configuration

Example job configuration:

    scrape_configs:
      - job_name: node
          http_sd_configs:
          - url: 'http://<HOME_SD-SERVER>:40404'


## Nodes configuration

Use any solution to automaticaly post some json to the server.

### Linux:

    crontab <<EOF
    */3 * * * * curl http://192.168.2.100:40404/ -H 'Content-Type: application/json' -d '{"name": "MYLAPTOP","port": 9100,"job": "node"}'
    EOF

