package homesd

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHappyCase(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer func() {
		state = map[string]Endpoint{}
		ts.Close()
	}()

	body := []byte(`{
		"name": "R2D2",
		"port": 9100,
		"job": "node"
	}`)
	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(body))
	if err != nil {
		t.Error(err)
	}
	if res.StatusCode != 200 {
		t.Error("Expected status code 200, got ", res.StatusCode)
	}

	res, err = http.Get(ts.URL)
	if err != nil {
		t.Error(err)
	}
	if res.StatusCode != 200 {
		t.Error("Expected status code 200, got ", res.StatusCode)
	}
	if res.Header["Content-Type"][0] != "application/json" {
		t.Error("Expected content type code 200, got ", res.StatusCode)
	}

	bodyBytes, err := io.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
	}
	expectBody := `[{"targets":["127.0.0.1:9100"],"labels":{"instance":"R2D2","job":"node"}}]`
	if string(bodyBytes) != expectBody {
		t.Error("Expected response body ", expectBody, ", got ", string(bodyBytes))
	}
}
