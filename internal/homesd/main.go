package homesd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

var state = map[string]Endpoint{}

type Endpoint struct {
	Name       string    `json:"name"`
	Port       uint16    `json:"port"`
	Job        string    `json:"job"` // "job" label
	address    string    `json:"-"`   // IP from http.Request.RemoteAddr
	lastUpdate time.Time `json:"-"`
}

type HTTP_SD_Entry struct {
	Targets []string          `json:"targets"`
	Labels  map[string]string `json:"labels"`
}

func Server(port int) {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(fmt.Sprint(":", port), nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		UpdateHandler(w, r)
	case "GET":
		GetHandler(w, r)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprint(w, "Method Not Allowed")
	}
}

func GetHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	response := []HTTP_SD_Entry{}
	for key, endpoint := range state {
		oneHourAgo := time.Now().Add(-time.Hour)
		if endpoint.lastUpdate.Before(oneHourAgo) {
			delete(state, key)
			continue
		}
		target := fmt.Sprint(endpoint.address, ":", endpoint.Port)
		entry := HTTP_SD_Entry{
			Targets: []string{target},
			Labels: map[string]string{
				"job":      endpoint.Job,
				"instance": endpoint.Name,
			},
		}
		response = append(response, entry)
	}
	bodyBytes, err := json.Marshal(response)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err)
		return
	}
	w.Write(bodyBytes)
}

func UpdateHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	if r.Header["Content-Type"][0] != "application/json" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Invalid content type")
		return
	}

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err)
		return
	}

	request := Endpoint{}
	err = json.Unmarshal(bodyBytes, &request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err)
		return
	}

	request.address = stripPort(r.RemoteAddr)
	request.lastUpdate = time.Now()
	state[request.Name+fmt.Sprint(request.Port)] = request
	w.WriteHeader(http.StatusOK)
}

func stripPort(address string) string {
	return strings.Split(address, ":")[0]
}
