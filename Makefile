.PHONY: bin
bin:
	GOOS=linux GOARCH=amd64 go build -o bin/linux_amd64/home_sd -ldflags="-s -w" ./cmd/home_sd
	GOOS=linux GOARCH=arm go build -o bin/linux_arm/home_sd -ldflags="-s -w" ./cmd/home_sd
	GOOS=linux GOARCH=arm64 go build -o bin/linux_arm64/home_sd -ldflags="-s -w" ./cmd/home_sd
	GOOS=darwin GOARCH=amd64 go build -o bin/darwin_amd64/home_sd -ldflags="-s -w" ./cmd/home_sd

.PHONY: test
test:
	go test ./...